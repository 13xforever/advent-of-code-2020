﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture(TestName = "Day 24: Lobby Layout")]
    public static class Day24
    {
        /*
        /\/\/\/\/\/\/\/
        | | | | | | | |
        \/\/\/\/\/\/\/\
         | | | | | | | |
        /\/\/\/\/\/\/\/
        
        ^1
        |
        -->2
        */

        private static (int x, int y) ParsePath(in ReadOnlySpan<char> input)
        {
            var (x, y, idx) = (0, 0, 0);
            while (idx < input.Length)
            {
                (x, y, idx) = input[idx] switch
                {
                    'e' => (x + 2, y, idx + 1),
                    'w' => (x - 2, y, idx + 1),
                    'n' => input[idx + 1] switch
                    {
                        'e' => (x + 1, y - 1, idx + 2),
                        'w' => (x - 1, y - 1, idx + 2),
                        _ => throw new InvalidOperationException("Unknown token " + new string(input.Slice(idx, 2)))
                    },
                    's' => input[idx + 1] switch
                    {
                        'e' => (x + 1, y + 1, idx + 2),
                        'w' => (x - 1, y + 1, idx + 2),
                        _ => throw new InvalidOperationException("Unknown token " + new string(input.Slice(idx, 2)))
                    },
                    _ => throw new InvalidDataException("Unknown token " + input[idx])
                };
            }
            return (x, y);
        }
        
        private static HashSet<(int x, int y)> ParseInstructions(in string input)
        {
            var lines = input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
            var result = new HashSet<(int x, int y)>(lines.Length);
            foreach (var line in lines)
                result.Flip(ParsePath(line));
            return result;
        }

        private static int GetNeighbourCount(this (int x, int y) cell, in HashSet<(int x, int y)> field)
        {
            var result = 0;
            var (x, y) = cell;
            if (field.Contains((x - 2, y)))
                result++;
            if (field.Contains((x + 2, y)))
                result++;
            if (field.Contains((x - 1, y - 1)))
                result++;
            if (field.Contains((x - 1, y + 1)))
                result++;
            if (field.Contains((x + 1, y - 1)))
                result++;
            if (field.Contains((x + 1, y + 1)))
                result++;
            return result;
        }

        private static void Flip(this HashSet<(int x, int y)> list, in (int x, int y) item)
        {
            if (!list.Add(item))
                list.Remove(item);
        }

        private static bool ShouldLive(in (int x, int y) cell, in HashSet<(int x, int y)> field)
        {
            var nCount = cell.GetNeighbourCount(field);
            if (nCount == 2)
                return true;

            return nCount == 1 && field.Contains(cell);
        }

        private static HashSet<(int x, int y)> SimGameOfLife(this HashSet<(int x, int y)> cells, in int rounds)
        {
            var back = new HashSet<(int x, int y)>(cells);
            var front = new HashSet<(int x, int y)>();
            var check = new HashSet<(int x, int y)>();
            for (var round = 0; round < rounds; round++)
            {
                foreach (var (x, y) in back)
                {
                    var n = (x, y);
                    if (check.Add(n) && ShouldLive(n, back))
                        front.Add(n);
                    n = (x - 2, y);
                    if (check.Add(n) && ShouldLive(n, back))
                        front.Add(n);
                    n = (x + 2, y);
                    if (check.Add(n) && ShouldLive(n, back))
                        front.Add(n);
                    n = (x - 1, y - 1);
                    if (check.Add(n) && ShouldLive(n, back))
                        front.Add(n);
                    n = (x - 1, y + 1);
                    if (check.Add(n) && ShouldLive(n, back))
                        front.Add(n);
                    n = (x + 1, y - 1);
                    if (check.Add(n) && ShouldLive(n, back))
                        front.Add(n);
                    n = (x + 1, y + 1);
                    if (check.Add(n) && ShouldLive(n, back))
                        front.Add(n);
                }
                (front, back) = (back, front);
                front.Clear();
                check.Clear();
            }
            return back;
        }
        
        [TestCase(TestInput, ExpectedResult = 10)]
        [TestCase(RealInput, ExpectedResult = 465)]
        public static long Part1(in string input)
            => ParseInstructions(input).Count;
        
        [TestCase(TestInput, ExpectedResult = 2208)]
        [TestCase(RealInput, ExpectedResult = 4078)]
        public static long Part2(in string input)
            => ParseInstructions(input).SimGameOfLife(100).Count;

        private const string TestInput = @"
sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew
";

        private const string RealInput = @"
neenwnenwwnwnwnenwsesenwnwsenwnenwwnw
newnwnenenwnwneeneeswnwnwnenwneswsenwne
wenwswnwswwnenwnwnwwnwswenwneswnww
seneeswwneneneeneeneswneneneeswnee
swseswswneseswwenwneseswswswswnwsesesw
senwnwnwwwseneseenwnwsenwnwnenwnenwnw
seeneeseseseeeesesenweweseswesesese
nwwwnwnwwsenewsenwnwnwwwwwswew
swnwwsewnewwwsenenwnwwweweswwne
sweseseweseeesesenwenwseswsesenwewsw
neswwnwswnenenwsewwwnweneseenenwnw
eweeenweesweenweeeswneeeene
wswswswswweswsww
senesenwsenweneseneswwsenwswsewwsene
wnenwneenwneweeseneseneeseeneneenee
neeeeeeseseeeeeeewewneeeee
nwsewnwswnwnewsewwnwnweswwwnwwew
wnwnenesenenenwsenw
nenenenenewnenenenenwnenwnenenwnesenenese
senwwwswnwweewseswnewwewenwse
seswseseswswswswswnwswswseswswenwsesesw
eseneneneswnenewnenenenewsenenenwnene
nenwneneneneseenenenewsene
eseeenweeeneeeeeneee
nesesewseseneseseseseseswwsesewseesesee
swneseeseseseweneseseneneweseseswse
nenwneneneneenenenwneneneswsenw
eseesesewseseesenenwenwseseesewse
neweswswnwsenwwnesenwewseneseneseww
nenwneseweneseenenewnenewnenenewsw
senenwnwswnwnenwnwnwewnenwneenwnenwnw
wnwnwneseenewnwswwnwwewwsweww
esweneweewnewnewesweeeseeesene
nwnwnenenewnenenenenesenw
swwwswswswnewseenwwwseswswswwswwsw
seswswsesenwswswswnesweswswwswseswneswsw
swewswwneswswswwswswsw
wwseseneswewwswnwswwwnwwswswswnee
wnwwwwswnwnwnewnwwwnwwweenwsw
seseseswswswwswseseseseswneseswnesesew
seeseeseeswsenwnewseswswsewsewseswse
wweneeeneeneenenewneneneswsenene
enwwsewneneseeneneewesenwswwnwsw
nwwwnwnwnwnwnwwsenwwenwwwnwsewne
swswswseswswseswnwswnwsw
wswseneewswesenwwswnwswswwswnewswsw
seeeesenewnweeeseeesweenwseenee
seswnweesesewseseseswsenesesewsesenese
swneeneseenwnenwswenenwwseswswesenwe
swswnwsesenenwsenwsesesesesewswswsesenesw
swsesenweewsesesesesese
nwneeeeeseeeeesewseeseswwee
weeweeeenweswseeeeeseeee
ewwwwwnwwww
enwsweneeneeneseneneneeneswnewneese
wwswsewwwnwnwwnwnwsenwnwnwnwnwwne
seswsesweswwseswswwnwnwenenenwsenwnew
swwewnwswneswwseswwww
neneenewneneneneeeneneseweeneenee
nweneneeneeeeneneseneswneneenewnene
nesesenweneneswswswswwswswwenenwsww
swseeseseneseseseseswesesesesesesenwsese
senenewewnenenenewneswneene
nwnenenwnenesenesewsewnwsenewnwnwnee
nenenesenwwnenenenenwnenenenwne
seesenesesesesesesesesesesewseswsewse
swsenewswseswnwsewnesewwneseese
senwwnwnwnwwnwsenwwnwnwsenenenwswewnw
wseeswnwnwswnwnenwswseenwnewnwnwnew
nwwnwswnenwnwnwnwnwnwnwnweswnwwwnwnwnw
weswnwswweewneswwwwswwswnwsww
nwswswsenwnwnewwswsesweneewwneswe
nwwnwnwswnwenwnwwwswwwwwewwnwnww
nenenwnweneswneneseswenwneneswnewwneswne
swnenewneewneneneese
seeeeneeeseeenewnwwesweewse
neswsenewswswswsewswnesesewswswswswsw
nwnwwneneeswnwnwnwnwnwnwswnwnwnwwnwswnw
eswwnwnwnwenenwnwneswnenwnwnenwnenwnw
swnenenenwnenwnenewsweneneneneneneene
seseseseseswseseswsesesenwseenwseseswsese
eneseeswneneseeswnwsewwewenwnenew
eeenwnwsenwseesenweeweesweese
wnweseeseneseeeseseeswnwseseswsese
nwwswwseswswwswwwseneswwneswwwse
nwwseswneeseeneewsesesesewesesesese
wewwswseneswnwswswswwwswswnwswsew
nwnwnwnwnenwnenwnwnwnwenwnwnwnwnwneswswnw
nwweeeseseseenwneswswnwsenwnewswnwse
wnewwsenewwnwswnwswsesweeweswsw
nwneenwnwnwnewnwnwnwneswnwnwneswnenwnw
wswwswwswswswnwnwswwswweswwswew
nwsesenwsenwseseseseesewenwsewswseswe
swseseseseswwseseseseswswnewesw
nesenenwsenwesenwsesewneswseswsesesese
wwwswsewwnwewswwswnwwwswweneww
swswswswswswswswwswwswswswwneswsw
enwwwnwwwwwe
neneneeneneesenenwneswnenewnwnwneswnwne
swswnwswswneswnwswswswsweswswswswswswswse
nenwneneneeneneneswnewenewsenenewe
wswseenwnwwnwwnwwwwwnwwswenwne
swswswesenwseswswswwswwseswswnwsweswsw
wwwnewsenwwnwnwsewnwnwwnwnwnwsenewnw
eseseseseneesesesenesewseseseseseesw
wenwnwwnwwwwswewwwwenwwww
swenwnenenenwnwenewnwenwsweswneswnww
wwnwseweswsenwsesweseswseewswsenenesw
wnewwwwwwwwsewswsewswwwnesw
seeeeneneeneewe
eesesewseswswwwnesesenenesenwsesesw
wneswwnwnwnwnwenwnwswwwwwnwwnwwnw
seseseseeesenwswwsesesesesewe
nwnwnwwsewswwnwwwnwwnwenwenwnwwwnw
sewsenwwseswenwsenwsesenenesenwseseneswse
eeseeweseeseseeweseeneeeee
nwnwnwsenwnwnwwnenenwnenwswsewsenwnww
newnenenenenenenenenenewneeneseneneneswne
nwseswneesenwnwnwnwnesenwnwswneseneenw
wwseneneswwnesenwsesewwneneswwnww
wseesesenwseseseseesewsesenwseseesee
swwswwwwnwswwwwwsewwsewneswswne
swswsewswwwwwswwswnewsewwwwswne
nwnwnwnwswnenwnenenwnwnwnwnwnwnw
swseswswneswswswwswseswswswseswswswnesw
eeneneeneswneneneswneeneeenenenenwne
neswswswnwwwswwnwswswswwswweeswswsw
nwwwwswwenewswswwwnwnweswnweww
swsenewwswesewnwwnewneewwnewwse
wsenenwnwsenwwnwsewwnwwnwwneenw
swenenwnwnenwnwnenewnenenw
nwwenwwnwnwnwwnwswnwnwnenwnwwsewne
eseswsewseneswsenw
nwneeneneseenenenwwnwseswneneeneswnee
eneeeeeeswseeneweeenwseeenenee
nenwnenwnwnewneneneenwneswnenenenenene
nesenwwswsewnwnwewnwwswwnwwnwwenw
seesewseswswwseswnwwswseswseeesee
swwwwnwwswwweswswswsw
nenwnwnwnwnwnwnwnwnwnwsesenwnwenwswnwnwnw
swneneswnenenenenenenwsenwnenewnenwnwnwse
wneweeeeeeseeeseseseseseesesee
newnwneeewenwweswseseneswneeenwswe
seseseseeswswsesenesenewseswnenwwsenenw
wswwwnwswswsweewwwwswwwswswsw
swnenwnwneneseeeenenwwnenenenewnenwesw
nwwneneswseswnenwenwswnwnesewwsenwnwnwsw
swnenwswewsenwseewwnewswswnewwww
sesesesewesesesesenwneseseswseesesesese
wnwewwnewwwseswwwwswnewwww
swnwweeeneeeneneeee
swnenwneseswwswnwseneeswwnwwnenwnww
nenewnenenenewneneeenenwneneneenesw
enwenweswnenewesenwesweseeseweese
swewseswsweswswswswsewseseswseseswnesw
nesweenenwnweeneneneneseeneneeenewsw
nwnwnwnwwsenwnwnwnwnwnwnwnwnwnwnwnwnwe
swswswswswnwswesweswswswswnenwswswseswse
swsenenewwwnwwswnewwwseswswwswenese
nwsenwnwewwewwnwnwwnenwewnwnwse
swnenwnwwwswewnwnwseene
wwnwnwsenwnwnewnesenwnesenwwsewnwnww
nenenenenewneeneewseenwswneneneenee
senwseseseseseseswswseeseseswsese
nwnesenwnwnenewneneeswnenenwnenenenwnesw
seswwnenwenwwenwnwnwnwwwneswwwesew
neneeswwnenenenenenenenenenenenenewe
seseseseeswsenwswseseswseseswseneswswsw
enwnwsewsewneeeenenwseneeeneseene
nwewnwnenenenewneseenwnenenenenenewne
nwswneneneenenenesenwnenwnenenenenewnene
esweswnwswwswswwwwswswwswswesww
wwenesenwsenwwwnwnwswnwenwnwnwnwnw
seswwseseswseseseseswnwseneseseseswsesesw
eenewnwnwnwnenwnenenenwwnwnwnwnw
swwnwswswneneeswwswwwswwswswswsenese
nwswseseeseeseseseseseneeeneweesee
wswewswwswswnewswseswswewswswnwsene
nwneswnwnwneeneneneneneswnwnwnwnwnwnwnw
wwwwwwwewsewsenwnwwwwwwww
eewseseweseswsesesenwseneenwee
esweeneeeeweeeneneenee
eeeseenwseeseeeeswseeeneeese
eneneneneneeeeeeneneeeeneseesww
nweeeeneswenweeeeneneeneneswe
ewwnwsewwwwwwswnwnew
neseswswswseswseseswsewsenwsweseswswnwse
wwwwewwwnewwnwewsewsww
wwwswswnewwswwwswswsenwwswseswww
nwsesenwswsenwseswse
neneswenenenenwneneenewseneneneneene
wnweneeseeeeseeseseeseswneseesese
enewneesweeeeeseweeeeseenwnw
senwwnesweeseeeeeeeweneseseee
swwwwswwwswwswswwswneswswsw
seseseseseseneesesesesewse
swnwswsweenwswseswseswswswswswswswseswswse
nwwneeenwenenwneneswwsewseswnenwnwswne
nwwsenwsenweswswneeswswsw
swwswswswwswwewnewswswwswwswswsw
wwswwswswsenwwsw
seeeseseesewseeeeseewseesesese
eneeswweeeeeeeneesenwneseswnwesw
wnesewnwwnwnwsesenewwwnwenwnwnwwnw
swswswswwweswwwwwswswswwww
swsenwseneswnenewswswseneseswsee
seeswseseeseswswseswswswsesesesenwsesenwse
swweswwnwseswwnewswenewnesewwswnw
eenwseswnwseseseweseenesesesesesesesee
wwneeswwwwwsewwwwwwewwwww
wswseseswswseswswneswseswswneswsesenwwsee
seewwwwswswwwnwnwneswswsw
ewewswweesesesenwewesenwnwesese
nwnwnwnwewnwwnenwnwnwenwnwswnwsenwnw
newneeeneenewneneneenesenenwwenene
neeesweseeenwseseeseesesweswnwwse
wwwwwwswnewnwsewwwnwwwnwsewnw
nenenenesweneneneneneneenwnenewnwnene
enewsenenesenenenenenwneneneesesenwwne
nwnwenwnwnwneweewnwwsweenwsewne
wnwnwnwswwenwnwswewwnwnwnwnwnwnwnwwnw
nwneseenwnenwnenwswnwnwwnewneenenwenw
nweeweeewwnwneseenwswseseswnwsese
wswewswwwewnwnww
seswwsesenewwswnesesesweesesesesese
sweeenwnweeeesweeeeweswenw
nwnwnwnwsenwnwswnwswnwwnwnwnwnwsenenwe
wwewwswwwneswnwsewswwwwwswsw
swwwwwwwwwewnwwwwwwnw
sewwwswwwwwnwnwweswwwswwwwesw
nwwseneesenewswnwnenewwwesenwwnesene
nwswswnenenwnwnwnenenesenwswwnenwnenenw
nwnwnwnwnenwnwnwnewsenwnwnwnwnw
swswwseswswseswswsweseswswseseswsw
esesenweeeesewseseseseesenweswese
nwnwwswwwnwesewwneweswswnewnewww
swseseswneseeswsenwseseswseseswnwsesese
neneseseneenwswnenenwenenwnwswswnwnenwnw
nwnenenwsweneswnwnewswneneneswnenwnesese
nwsewnweenwnwneenwnwwsenwwsewnwnw
nwswwenwneneneneenwnwnwnesenenwnenwnw
seswnenenwnwwwnenweseswewnwwsesenw
neneseneneswneswnenenenwnwnwswnewnenenw
seesweeseeeseeeenwsesese
nwnenwenwnwnwnwnewnenwnwnwneswswnenw
seeeeeeeneeneseeweeeseeseewe
enenwwswnwnwnwwnenwwwsenwnwsenwnwsenwnw
wesenesewseseewse
nenesenenenenenwnenenewnenenwnwnesenenenw
neswnwswswwswswswneewnwweswnwwwwe
esewsenwweseseeseeweeewnewne
nwnwnenwwnwwnwswnenenenenesenwnwnenenwe
swseswnwswwwswswnenwnwswswesesewsenwnw
wesenwenwewwwwswnwneswwnenenwnw
seseseseseswsesesenwswseneseseseseseswwse
weenenewseswswneenenwwswwesesew
eneenenwnenenwnenenwewnwneneswnenwswnenw
swseswswseswswswswswswenwswswswseswnwswnwsw
eeeenwseeswneeesesw
nwneswswnenweneswnwwnwsewnweneenwwnw
eneneenenenwneswnwswnenwnwnenewnwnesene
swswsewswswswswswswneswsweewswwswswwne
wwwswwwwwwwwwswwswwnwneeww
seeseesesenwsenwseseseseseseseseswsesene
swnesweeeseswnwwnwneeenwnweee
nwwnewwwwsewwnwwnwneswnwwswesew
seseneeseeeseeseeseweeeswesene
seswwwseewnwswwwnwwswnewwswww
neeneneeneeenewneeseeeenesenw
wwnwwwwwnewnwwswwwwnww
sesenwseeseseseseseseseseseseseseseseswnw
neneeneeeneswneeneswnewenwwneee
nesenwneneneeseswsenenenenenenewnwenenwne
neenenenwnwsenesenwsenwnwswnenwne
swnwnwnweseseeeeeeeseseweswswseee
neneneneswnenenwneneneneneneeneneswnenene
newseneneenwnweweeeeseeeswew
eeeseeeeeeswseesenesesenwnwese
nwnewswnweneneneseenwswwnwswesesesene
neswesesesweenwseseeeeneseeeesee
eeesenweweeesenweseeeeswesesenw
seewesesenenwnewsewneeswnwsweesenw
nwwnesenwnenenwnenesenwnwnewnwnwnwnenw
sesenwseswseeseswnesweseseswnwwswseswsesw
nwneneneeswnwenwnwswnenwnenenw
eeseseseenewseswseswseeseneewee
swswsesesesewsenenwswweswswswsesenw
sewswswswswswswswneseswseswswsese
nesenenenwwnewneneneneseneseneeneswenene
nwswnwnwnwnwsenwneswwnewnesenenwneeneenw
swwwwwnwwewswnwwwwwenenwwsew
ewnwswswseenwsweeneneweeseenwwswne
nwseseswnwseseenwewseseseseseesesee
seeeseweseeeeeeneseeswseswsenw
wwewswneswswwwwwnesewwwswsww
nwnwwwnwwsewnwswnwewwnwnewwnww
swswswswseseswwswsesewswswswsweswnwnwene
nenwnwnwnwnwsenwnwnwnwnwnwwwnwnwnwsenwnw
nwswswnwnwsewsenwnwenwwnwnwnwnwnewnene
wswseeeswnwswseseneswesewswneswwsw
nwnwnwnwnenwnwswsenenwneeswsenenwnwswswnw
neewswneswnwseswnwnwnewnwwnwwwnw
nwnwseewneseneeeesewsesesee
nenesewnenenenenenenwne
seeseseseseswsesesesenwseseseswsesew
sewsenwwneneneeswseneeeeenenenwe
senwenwnwnwwenwnwnwnwnww
swnwsenwnwnwsenwswnenwnwswnwenenwnwsenene
seesenesesewsewnwnwswwsweenesewswsw
swenwnenewnenenwneswnwnenenewswnenee
wseesenwnwneswswsesesweswswseseswswswsw
eseseseseseseeseesesewseeseenwwee
eeneweseesweeeewneeewenenw
eeeneeswneeeneeeee
neneneeneswnwnenwnewnwneenewseenewew
eeswnwnenwnwneswnwnwwnwnwnwnwnw
swwswwwwwewswnwnwswsesw
seseseseswnwswseswne
sesesesenwnewenwnweseseseseeseseesew
wnesewwsewwwwwwnwsweswwnww
nwnenenwnewnenwnwseswneswnenenenenenwnwnwnw
nwseeswswswswswneseseswswswswswswsewswswsw
sesesewswneswseseseswseswsesesene
eseeseeseeeewsenwseeeeswsenwseese
eswswenenwnweenesenwsw
senwnwnwnwnwnenenwnenenwne
neswnwnwnwsenwnewnwnwneeewnenwswnenenenw
seeseeeseeneseswnewnewseeseeseesw
swswseswwswneswwswswswswweswswnenesw
eeenweseeseseeeeeseneeeswwe
nwnwnwnweeswnenwnenwnwnenwwnwswnwnewsene
neneswswwswneswswnewnwswswe
wwwwwwwweewsenwswwnwneswnwwsw
ewswseswnewsenwnwnwsenwnwsewseeseesw
sesesesenenwseswseswseseseseswwseswswe
neseweseseseseseseesewseseesesenese
nenenenenenesenewenenenwsewsenenene
weenesweseswesenwseeenwsese
weeseneseswswseswnwseseseesewnesesene
eeseneswswsweeeeseenwsenwsesenweew
swswnweswswswwseswswwwnwswwwwwswsw
seswswnwwnenwnenenwnwnwnwnwnwnwnw
swswseswswswswnwnwswswswsweswswseswswsw
ewswswswswswnwnenwswsesweswseswswswsww
nwwswwwwnewwwwnwnw
wnwewsenwwwwwwwwwewnewwww
swwsewwwwwwnenwnwwneewswwene
sweesweswwwneseswswnenwwseseewwse
swswwswneswweeswnwwenwswswseswswsw
nenwenenwswsenwnwswnenwenenwnwnwswswnenw
wwsewwwwnewwww
nweseeneeneswenesenwenwneneswnwne
esweswswswswseseswnwswswwswwnwwswnw
wwwsenwnewswwnwswwswwnwseseewwsw
seseswneswneseseswnenwswswswwseswswswse
swneeeneeneswnweneneneswswneneeneene
swswwswnwwnweeswswswwswwswswswswene
swswswwswswswswswnwneswswseswswswswswsw
seswwnwseswseswswswswsesesenewseseswsesene
enwnwseeneneeweneseeswnewnenewseene
nwswnesenwnewswswweenewewsesesesewne
enenwneseswneeneneenwneeenenesweee
nwnwseswnwnwwnwnwnwnenwnwnwnwnwnwnwnw
seseeseseseseeneweeeeweseseesee
enwnenwnwnewnwnwnenwenenwnwnwwnwnenw
eneseswnwnwnwnweswnwsewneswnwsenenenw
enenenwnwswnenenesenesewnenesene
eeneneeewseneneeeneswnee
seseswswseseswnwswenwswswswswseseeswsese
nwewswwwswnenesewwswwnwwwwewwsw
nwnwnwnweswnwnwnwnwnwnwnwnwnwnwnw
sewnenwnewnwneseneseswsenwnwnwswnenesesw
swswnwswwneeneewwewswswswnwwsew
newnenwnenwnwnwnwnenesenwnwwenwnwnwnw
wswwswwwwwswewwwwwnw
ewenweeesesesewseneseneeseswew
senewwswswsewnenwswewwwswwenenese
seswseeseseswesewseenweesenenesese
seeweseeneeeeeseenweeeewsee
neenweenewneesw
swnesenwsweeeswnwswwwseswneeswww
nwnwnwnenenweswsenwnwwneswnwnwnw
nesewswwwwewsww
seseseswseswswsesenwseswswswsesese
neenenenwneneeneenwswneneneneseneneene
nwnenwnwsewnwnenwnwnenenwnwnesenwnwnenwne
enenwneswwwnewswwswwswwwnwnesenese
seewenwseewswnesesesenwswswseswsewsw
swseswneswseseswswseswneswsenesesenwwsese
neneneewneneneneneneneseneswnwsenwneene
wnewewwwwwwsewsewwwwwneww
wswseeswsesesenwsesenwseseeswswswswse
eeenwswnwnesweeeeseeeeewesewe
swwwswwsesewwneenwswnenewsewwww
swswswswswseswswswswswneswsw
nwesewnwnwnwwnwnwnewnwnwwe
nwwwwneswwsesenesenwneenewwwwnw
seseswswswseneswswswswswnwswseseswsesewene
sweseseseseseeseseseseenwsesesesesesenw
swswnwswswswswswswwseswwnwswswswswswse
nenenenwnewsenenene
nwnwswwnwneeneswnwenesweswswwswe
nwnwnwnenwnenwnwneswneneswneneswnesenwswne
nwneseswswswswswseneswswwsesw
wnwswnwnwswswswswenenenwswswwsweswee
seswseseswseswseseeseswswneswswswswnwsese
seseneseseseseseesesesesesesesew
seseneswnwsesenwsesesewswseseswnw
nwswwnewwswwnewewwswesenwsww
swswswwseseseswwswswneswswswneswsweswsw
newnenenenwnenenwnwneeneswnenenwnenenw
swwswnweenewwsenwnenwenenweswsenwwne
seswnesesesenewseseseseseseswseswsewse
eseseeeeseeewwseesewsew
swswswswsweswswswswnwseswswwwswenenwsw
eswswswswswwswneseswswwswswswswwwsw
nenwswsenenwnenenenwnenenweswnenenw
swnwwewwwneeewwnwwswnwnwnenwsw
swwswseswswwswwwwswewnwsw
swwswewwswewnwnweewwnwneswwnw
senwnwseswswsesweswseeswwnwwesweswsw
wswnwwwwnwswwwwnewwswseswwsewsesw
swnwswswseswswswsesweseswseswsewsenwse
seswsenwswswnwswnwswswswenwewswswseene
eneweneswnwseeswneswwnwwsenwenww
eeeeenwswneseewseesenwewseneswse
nwnenwneneewnenenenwnenwnenwneseneseswnee
swneneewswswwswswewwswswnwnweswswe
sesesewseseseswseswsesesesesene
nwseeswseseweswswwe
nwnewnwewwwswwswwnwwwwswwnwe
wseneeeeneswweenwseeenwsewee
swsenwswseseswsesesenesesewwseneseswsese
seesesewsesesenewseseswseesenwsesesesese
swswsweseswnwswswswsesenw
neenwswswswswneneweswwneseeswneswsww
swwwsweswwnweswswwwnewwnwswwww
nwnwnwenenwnwneseswsenwsweswnwnwnewnw
wwwnewswwwswwwwwwwww
sweseseseseneneneseseseseseseseewwse
neeneeneeeweneneneseneneeenene
nwenwwwnwnwnwnwwnwnwnwsewnewnww
senwnwnwesweweewese
eseweeeeeseenweeeeeseewese
swneswsesesewswnwswseswneswseseee
neeseneeeneseeeenwswweeenenwnee
esweneenenwneeenesweeeeeeee
nwwwnwwwwwnwnwwwnwenewnwsewe
esenenweseeenweneneeneeneenenene
seeeeeeeeewenwseneeeeeee
swswnwwsenenwneneneneneseswenesenenew
senenwnewnwnwnenenwsenenwne
nenenwnewneneseneenenesenwnwneswnewne
sewswwneswwswwswewnwwswnwswwwsww
swswseswneswswwwswswnesewwwswswwwsw
swswneswwswwswswnwswwwswswswswseswe
nwenwwnwwnwnwnwswwnwwenwnwenwnww
swseswswswswwwnweswenw
senweswseseseswswswswseseswswwseswsese
neewwwnwnwwsenewsweswwnwswsesew
swseswseswneswseseseseseseseswnesesewswsw
wwwneenwwnwnwwneswwswnwsenwwnwswnw
neseseswwneseseseseswnwseswseswwseswsesw
nenenenenewwneeswweneeswweenwe
sweeswswswswswwwseseesenww
neneseneeswnewnwnewwsenwneneseenenene
nwnwsenwnenwnwnenwsenenwswnwnwnenwnwnwne
eeeeeeeswweeseneneeeeeneeenw
nenwnwsenwnenwnwwnwnwwnwnwnenwnwsenwnwnwnw
enwwseenwnwswsenwnwnwnwne
eeeeneneeeewseesweeeeeee
wnenwseweswnwnwnwwswewwnwnwnenwesw
eeneeweseeeneeseeneeeeweee
swnwnwenwwneseseesw
nwwwwswwwwwnwwnwnwwne
sewsweswenwnwneswwnenwnenewswesene
seeseeseswswneneeseswsesewswnwnwswsw
wseswnwnwnwseneenwsewsenwswew
wwneneswwswwswneswneseesenwwswsesw
wwwswnesenesewwwswswsewswswwwnw
nwnwswnwnwwwnwnwnwnwnwwnwnwnwewnwe
neneneneeswneneneneneneneneneene
ewseneseseseseeeseeseseseesesese
eeeeewnweseeesweewseeeeswnw
seswswswswwsweswswswswswswswsw
swswseseswwswsenwswswseswswseeseswswsw
swnwenwwwwwnwnewnenwswswenwsewnwnwnw
nwnwsenwnwswswwnwneenwnwseewnwsenwnese
enwnwnenwnwnwnenwnesenwswnenwnwnenenene
eswseswneenwenwnwseeewswseeneswnw
wwswnwswswswswseswswsweswswnewwsww
seeenwnenenenenesenenewneeenene
seswseseswswswswswseswswnwsw
nwswnwnesenenwneswnwnwnwnenwnwenwnwwnw
nweeseseswswswseswswseswseseseswnwseswsesw
eenweeeeeeneeeseeenwswnwese
swswswswswswswswsewnwswswswseswseswswswne
eswswnwnwnesweenwnwnww
neweseseneewsenweswsewseseeswwee
nwsenwenwnwnwwnwseswenwnwnwnwnenwwew
nwwnwnwwweseswnwww
swwewnwswswwwnenweewew
nwnwnwnwwnwswnenwsenenwwwnwsenwww
seeneseseeeeweweseenweseseswe
eesenwwewseesesenweeeseeenesw
swswswswswswswswswswswswnwswwswswswswee
wneneseneneesenenwneenenenwne
nenewneenenenenenenenenwnene
nesenwswnwnenwnwnwnenwswnwswnwnenene
nwwseneneneseneeenenenesweneneenwne
eseeenweesweeeeeneseeewenese
swwseseseswneswseseswswsweswsenwseswswse
seeeesenwswesewswnwseseseneenwwe
sweneeeweenenwneeeeeseseeseeew
neeswseweeeseseeeeneesenwsese
swwswwwswswswswswswswwneswwswswnese
senwneneswneeeswneneneseneneenwnewnenene
wwwwswwwwsewwwnww
ewswwwswswwswnwswwwwsewwwwnewsw
wnwswswnwneseseneesenenwsewnw
enwsweseeeeeeseneseseenweeeee
enewneeneeeeeneseeeeww
eweweneeseseeseseeeeeeeeee
swsenwweewwnwwnw
nenwseweneneseswnenwnewswneeew
eeswneneneseeneeseenewenwnweene
eeeeeeneneeesw
";
    }
}