﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture(TestName = "Day 18: Operation Order")]
    public static class Day18
    {
        private abstract record Token{}
        private sealed record ArgumentToken(long Value) : Token;
        private sealed record OpToken(char Op) : Token;

        private static long Parse(in ReadOnlySpan<char> input, in ICollection<(char op, int priority)> opPriority)
        {
            var stack = new Stack<List<Token>>();
            var expr = new List<Token>();
            var span = input;
            while (span.Length > 0)
                switch (span[0])
                {
                    case >= '0' and <='9':
                    {
                        var i = 1;
                        while (i < span.Length && span[i] is >= '0' and <='9')
                            i++;
                        var arg = int.Parse(span[..i]);
                        expr.Add(new ArgumentToken(arg));
                        span = span[i..];
                        break;
                    }
                    case '+':
                    case '*':
                    {
                        expr.Add(new OpToken(span[0]));
                        span = span[1..];
                        break;
                    }
                    case '(':
                    {
                        stack.Push(expr);
                        expr = new();
                        span = span[1..];
                        break;
                    }
                    case ')':
                    {
                        var arg = Eval(expr, opPriority);
                        expr = stack.Pop();
                        expr.Add(new ArgumentToken(arg));
                        span = span[1..];
                        break;
                    }
                    case ' ':
                    {
                        span = span[1..];
                        break;
                    }
                    default:
                        throw new InvalidDataException("Unexpected token: " + span[0]);
                }
            return Eval(expr, opPriority);
        }

        private static long Eval(List<Token> expr, ICollection<(char op, int priority)> opPriority)
        {
            var priority = opPriority.Max(v => v.priority);
            do
            {
                if (expr.Count == 1)
                    return ((ArgumentToken)expr[0]).Value;

                var priorityOps = opPriority.Where(v => v.priority == priority).Select(v => v.op).ToArray();
                var opIdx = expr.FindIndex(0, t => t is OpToken op && priorityOps.Contains(op.Op));
                while (opIdx > 0)
                {
                    var arg1 = ((ArgumentToken)expr[opIdx - 1]).Value;
                    var op = (OpToken)expr[opIdx];
                    var arg2 = ((ArgumentToken)expr[opIdx + 1]).Value;
                    var arg = op.Op switch
                    {
                        '+' => arg1 + arg2,
                        '*' => arg1 * arg2,
                        _ => throw new InvalidOperationException("Unexpected op token " + op.Op),
                    };
                    expr[opIdx - 1] = new ArgumentToken(arg);
                    expr.RemoveRange(opIdx, 2);
                    opIdx = expr.FindIndex(opIdx, t => t is OpToken ot && priorityOps.Contains(ot.Op));
                }
                priority--;
            } while (expr.Count > 1 && priority >= 0);
            Assert.That(expr.Count, Is.EqualTo(1));
            return ((ArgumentToken)expr[0]).Value;
        }

        private static readonly (char op, int priority)[] Part1OpPriority = {('+', 0), ('*', 0)};
        private static readonly (char op, int priority)[] Part2OpPriority = {('+', 1), ('*', 0)};
        
        [TestCase("1 + 2 * 3 + 4 * 5 + 6", ExpectedResult = 71)]
        [TestCase("1 + (2 * 3) + (4 * (5 + 6))", ExpectedResult = 51)]
        [TestCase("2 * 3 + (4 * 5)", ExpectedResult = 26)]
        [TestCase("5 + (8 * 3 + 9 + 3 * 4 * 3)", ExpectedResult = 437)]
        [TestCase("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", ExpectedResult = 12240)]
        [TestCase("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", ExpectedResult = 13632)]
        [TestCase(RealInput, ExpectedResult = 4940631886147)]
        public static long Part1(in string input)
            => input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries)
                .Select(l => Parse(l, Part1OpPriority))
                .Sum();

        [TestCase("1 + 2 * 3 + 4 * 5 + 6", ExpectedResult = 231)]
        [TestCase("1 + (2 * 3) + (4 * (5 + 6))", ExpectedResult = 51)]
        [TestCase("2 * 3 + (4 * 5)", ExpectedResult = 46)]
        [TestCase("5 + (8 * 3 + 9 + 3 * 4 * 3)", ExpectedResult = 1445)]
        [TestCase("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", ExpectedResult = 669060)]
        [TestCase("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", ExpectedResult = 23340)]
        [TestCase(RealInput, ExpectedResult = 283582817678281)]
        public static long Part2(in string input)
            => input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries)
                .Select(l => Parse(l, Part2OpPriority))
                .Sum();

        private const string RealInput = @"
7 + (9 * 8 + 5 + 5 * (3 * 4 * 7 + 6 * 4)) * ((3 * 6 + 3 * 4 * 7 * 4) + 4 * 3 * 5 + 5 * (5 * 6 + 7)) * 2 + 6 * 4
9 * 4 * ((9 * 8 + 9 + 2 + 9) + 2 * 9 + 2 + 2) * 5 * 6
3 * ((9 * 3 * 8 * 6 * 6 * 7) + 8) * 2 * 9 + 4 * 8
(3 + 4 + 4 * 4 + 9) + (7 + 6 + 2 * 8) * 9 + 7 * 8
9 + 6 + 6 + 9 * 9 * 5
(3 * 5 + (4 + 3 * 7 * 8) + 7 + 8 + 2) * 8 * (3 * 4 * 8) * ((4 + 5 * 2) * 7)
6 + (5 + 8 * (6 * 8 * 9 + 9 + 8 * 9) * 4 + (5 * 9)) * 2 + 2
(6 + 8 + (6 * 6 + 2) * 6 * (7 + 7 + 8 + 9)) * 3 + 8 + (6 * (8 + 8 + 9)) + 2
9 + (4 + 6 + 5 * 9 * 4)
(5 * 3 * 4 * 4 + (6 * 4 * 5)) * 2 * 9
(2 * 3 * 9) * 2
6 + (4 + 8 + 2)
(9 * 7 + (2 + 9 * 9 * 9 + 3) + (7 * 3 * 4 * 2 + 3 * 7) + (5 + 9 + 4 + 3) + 4) * 9
(2 + 2 * 5 + 9) * ((5 + 9 * 4 + 7) * 2) * 7 + 4
4 + 2 + ((4 + 3 * 2 * 4 + 5) * 6 + 7 * (4 * 2) * 9 * (6 + 3 + 8 + 2 * 5))
(5 + 9) + 6 + 9 + 8 * 5 + 4
5 * 5 + ((3 + 5 * 4 + 7 * 9 * 7) + (7 + 2 * 3 * 4 + 8) * 6) + (5 * 9 + 9 + (3 + 5) * (8 + 2) + 5) + (3 * 3 * (6 + 3 + 7 * 8) + 3 + 2)
2 + 4 + 8 + 4 + 7 + (2 + (4 + 8 * 4 + 4 + 6) * 8 * (6 * 6 + 7 * 2) * 6 * (8 + 5 + 9 * 4 + 5 + 6))
7 * 4 + 8 + (8 + (7 * 3 + 4)) * 3
(6 + 7 + 8 * 4) + ((8 + 7 + 8 * 5) + 8 * 8)
9 * 5 * 8 + 2
6 * (9 * 5 * 9 + 9 + (3 + 7) * (8 + 9 + 3 + 5 + 2 + 7)) * (6 + 7 + 4) + (2 + (2 + 7 * 2 + 4 + 2) + 4 * (9 + 6 * 5 + 2 * 9)) + (8 + (8 + 6 * 5 + 6 + 9) + (2 * 7 + 2 * 9 * 9) + 7)
4 + 2 + 9 + ((3 * 6 + 8 + 4 + 5 + 4) + 4 + (5 + 4 * 8 + 8) * (3 * 4 * 8) * 7 + (8 * 3 + 8)) + 5
6 + 9 + 4
9 * 3 * 4 * 3 * 9
2 * 7 * (9 * (6 + 9 + 6 + 5 * 5 + 6) + (2 * 7 * 2 + 3) + 6 * 2)
(6 + 3 + 6) * 9 + 5 * ((9 + 9 + 7 + 5) + 4)
8 * 4 * (8 + (7 * 4) * 5 + 6 + 7 + (5 * 7 + 9)) + (5 + (8 + 7 * 2 + 2 + 3) + 8 + 6 + 2 + 2) * (5 + (7 + 4 * 2 + 6 + 6 + 3) + 3 * 8 + 7)
9 + (5 * 6 + 5 * 2 + 8) * 3
5 * ((9 * 5 + 9) * 9) + 4 * 8
4 * 5 * (5 + 5 + (2 * 9 + 3) * 4) * 7
9 * 8 * (8 * (9 * 4 + 7 * 2 + 3 * 8) + (2 * 7 * 2 * 3 + 7 * 2))
9 * 4 + 2 * 7 * (5 * (4 * 5 * 6) * 3 * 4 + 4) * (4 + 4)
2 * (5 + 6 + (2 * 3 * 3 * 2 * 5 * 5)) + 4
(5 * 7 * 8 + 4 + 6) * 2 + 3
8 * ((5 * 3) * (6 + 6 * 6 * 2) * 8 * 6 * 6) * 7 * (3 + 6 * 2 + (9 * 8 * 8 + 7) * (5 + 2 * 2 + 5) + 9)
7 * (7 + 5 + 2 + (5 + 9 * 4 + 5 + 3) * 8) + 2 * 8 * 6
2 * (6 + 7 * 7) + 3 * 7 * (4 * 7 * 6)
7 + 7 + 8 * (7 + 2 * (3 + 9 + 8 + 7 + 4) * 9) + 5
8 + 3 * 9 * (8 + 8 * 5) + 8 * 2
6 * 6 * 5 * (8 * 9 * 2 + 6 * 2 * (9 + 3 + 4 * 3 + 6)) + 2 + 4
3 + 8 + 7 + 3 * 5 + 9
(4 * (9 * 2 + 3 * 4) * 5 * 2 + 5) + (7 * 7 * 4 + (4 + 3 + 5 + 6 + 3 + 7) + 6 + 3) * (3 + 9 * 2 + 2) + 3 + 8 * (7 * 8 * 5)
8 * (6 + 6 * (4 * 3 * 6 + 9 + 9) + (2 * 3 * 2 * 4 * 2)) * (2 * (8 + 8) * 6 * 4 + 3) + 9 * 6
3 * ((7 + 6 * 8 * 9) + 7 * 7 * 2 + 3 * 7) * 8 + 9 + (2 + 7 * 6) * 3
4 * 8 + 6 + (3 * 3 + 8 * 7 * 9) + 4 * 6
2 * 5 * (8 * 2) * 4
((3 + 9) * 2 * 5 + 5 + 8 + 8) * 6 + 2
9 + 2 + 3
7 + 4 + 9 * 9 + (9 * 6 * 5 * (3 + 3 * 7 + 3 + 9 * 7) * 9 + 8) + 9
(6 + 2 * 9) + (6 * 6 + 2 + (6 * 6 * 3 * 6)) * 4 + 2 + 2
5 + 9 * ((3 + 8 + 7 + 3 + 9 + 3) + 8)
(4 + 7 * 9 * (4 * 8 + 7 + 2 + 6) + 2) + 9 * 9 * 2 + 8 + 8
(3 * 6 + 3 + 7 + 5) + ((2 * 3 + 2 * 3 * 7 * 5) + 6 + 6 * 9 * 7 * 8)
5 * 7 + 7 * (6 * 5 + 8 * (7 + 5 * 9)) + 8
(7 + 6 * (2 * 5) + 3 * 6) + 5 * 2 + ((8 + 9) * 6) * 2
4 + 8 * 3 * (7 * (5 * 5) * 3) + 3
9 + 7 + (3 + 7 * 5 + 3 + 2) + (8 + 7 * 5)
2 * ((5 * 8 * 2 + 5 * 2 + 8) * 2 * 4 * 3) + 5 * 5 * 9 * 4
3 * 6 + 6 + (8 + (3 * 8 * 4 * 7) * (6 + 5)) * 6
6 + (5 * 3 * 7 * 2 + (2 * 6 * 6 + 4) + 9) * 2 * 6 + 6
(4 + 9 + 9 * (3 + 6 + 3 * 7 + 8 + 5)) * 8 + 5 + 5 * (8 + 6 + 3) * 4
7 * 8 + (2 + (5 + 6 * 5) * 6 * 6 + 5 + 3)
7 + (3 + (8 + 7 * 5 + 5 + 5) * 5 + 4 + 8) + 9 + 3
7 * ((7 * 5 * 9 + 7) * 6 * 4 * 6 * 6 + 6)
7 + 5 + (3 * 3) * 6 + 5
(4 * (3 * 3 * 5 + 8 + 9) * 4 * 8 + 7 * 3) + 5 * 5 + (8 + 2) * 9 * 3
8 * (2 + 3 + 4) + 4 + 8 + (3 * 4 * (3 + 2) * 4)
9 * 2 * (9 + (8 + 6) * (7 + 9 * 5 * 6) * 8 + 7 + 7) + 5 * 2 * 9
7 * 7 + 7 + 2 + (5 + 2 + 6 + 7 * 6 + 8) + (9 * (6 + 2) * 3 + 9 + 4 + 7)
(3 * 4) * 9 + ((2 + 5 * 2 + 9 + 6) + 5 + 9 * (4 * 9 * 7 + 2 + 3 + 6) + 4 + (4 + 7 * 8 + 5 * 9)) * 9 + ((8 * 9 + 2 + 7 + 8 + 5) * 7 + 5 * 2 * 9 + (7 * 8 * 6 + 8 + 2))
((4 + 9 + 7 * 6 * 6) + 3 + 6 * 3 + 3 * 5) * 7 + 2 * 9 * 4
3 * 7 * 3
(6 * (9 + 8) + (3 * 4 * 8) + 9 * 4 * (8 + 6 * 8 + 8 + 3 * 5)) + 3 * 4 * 9 * 6 + 2
((5 * 7) * (9 + 2)) + (2 * 9 * 7 + 3) * 8 + (7 * 5 * 5 * (9 * 9 * 2)) + ((7 + 7 + 8 + 4) * (5 * 3 * 7) * 6 * (7 * 7) * (2 + 2) + 7) * 6
((9 * 9) * (8 + 3 * 5 + 9) + 5 * 7 + 3 + 8) + 2 + 7 + 3
8 * 5 * (8 + 3 * 6) + 4 + 8 + 5
(4 * 9 + (8 + 9 + 8 * 9 * 3) + (3 * 8 * 9 * 9 * 2) + 6 + 5) + 2 + (5 * (6 + 4 * 9 + 3)) + (4 * 9 * (9 * 8 * 7 * 6) * 5 * (2 * 7 * 9) + 5) * ((4 * 5 + 6 * 4) * 6 * 6 + 9 * 7)
(8 * 8 * 7 * 5 * 2 + (9 * 9 + 4 + 8 + 3 + 5)) + 5 * 3 * 3
5 + (8 * (3 * 3 * 2 + 7) + 9 + 2) * 8 * 4
9 * (9 * 4 + 2 + 9 * 5 + 3) * 3 + 3 * (5 * (9 * 5 * 8 + 2) * (3 + 8 + 3 + 2) + (6 * 5 * 9 * 5) * (4 + 2 + 5 + 2 * 9) + 4) * 5
8 + (7 * 7 * 9) * 7 * 8 * 9 * 3
6 + 9
(3 * 8 + (8 * 2 + 9 + 3 * 5 * 6) * 2) * 6 * 2 + 4 + 4 * (4 + 5 * 4)
(6 + 8) * 3 + 4
7 + 2 * 4 + 9 + 7
8 * (7 + 2 * (2 + 4) + 3 + 6 + 9) * 2 * 9
(6 * 9 * (2 * 6 + 2)) * 4 + 7 + 9
((2 + 7 + 7 + 4 * 2) + 2 * 9) * 9
3 + 8 + 9
5 * 9 * (3 + 4 * 6 + (8 + 3 * 3 + 5))
(4 * 4 + (8 * 6 * 7) + (7 * 5 + 9 + 6 * 7 + 8) + 8 * 7) * (6 + 2) * 5 * 3
6 * 3 + 5 * (9 * 2 + 8 + 5 * 9) * 8
(8 * 9 + 7 * 7 * 8 * 8) * 7 * 6
6 + 7 * 6 + 2 * (6 + 7 * 5 * 9 + 6 * 4)
4 + ((9 * 3 + 9 * 9 * 5) + 5) * 9
2 * (2 * 7) * 6 + 9 + (8 * 7 + (2 * 5 + 8 + 5) + 4 * 5 * (4 * 3))
(9 + 2 + (9 + 5 + 7) * 3 + 7 * 3) + 3 + 8 + (6 * 5 * 9)
(9 * (7 + 6 * 9) * 5 + 5 + 6 + 6) * 5 + 9
7 * 8 * 2 * 9
5 * 2 + 7 + ((6 * 4) + 8 + 2 * 3 + 6) * 9 * 2
((6 * 8 * 6 * 5 + 3 * 4) * (4 + 5) + 4) + 9 + 4
4 + 7 + 7 + (4 * 7 * 3 + 9 + (6 + 5) * 9) + 5
(2 + 4 * (8 + 6 * 5 + 4) * 2 + (9 + 6)) * 2 + 8
(2 * (3 + 2 + 9 * 4)) * 8 * 9
3 * 3 * (9 + 6 + 7)
6 + 2 * 6 * 5 + (4 * 7) + 8
((6 * 4 * 8 + 2 + 4) + 5 * 5) + 4 * 4 * 9 + 4
(7 + 9 * 2 + 8 * (7 * 3)) * 6 * 9 + 4
(5 * 8 + 6 + 2 + 3) + 6 + 8 * (4 + 4 * 7 + 8 + 7 * 4) + 3
(8 * 5 + 4 + 3 * 2) + (8 + 5 + (9 + 7) * (5 * 9 * 2 * 5 + 9 + 9)) * (7 + 8) + 2
9 + (2 + 7 * 2 * 7 * 8) + 9 * 5 + 4 + 9
5 * 7 * ((5 + 9 * 5 * 5 + 9) + 2) + 4
7 + 7 + (4 + (3 * 6 * 9 + 8) + 2 + (9 * 6 + 5) * 2)
9 * 8 + ((7 * 5 * 2 * 3 * 3) * (4 * 8 * 2 * 9 * 8 + 5)) * 6 + 4 * 9
(8 * 5 * 8 * (2 * 5)) * 6
((9 + 7 * 5 * 6 + 3 + 8) + 8) * 6 + (5 + 2 * 2) + 4 + 9 * ((4 * 5 * 7 * 9 + 4) + 9 + 2)
((3 + 3 + 2 * 4 * 4) * 7) * 3 * 8 + 4 + 8
2 + (2 * 3 * 7 * 2 * (3 * 4 * 6 * 2 + 2 + 5) * 7) * 3 * ((8 + 5) * 4 * 4)
(7 * 3 * 9 + 7) * 9 * 6 + 2 + 6 + (8 + (5 + 6 + 9 * 7) + 6 + 4)
(8 * 4 * 6 * 7 * 2 * 4) * 5 + 2 * 3 + 9
7 * (7 * 3 + 8 * (5 * 4 + 8 * 8) + 7) + ((3 * 6 * 3 * 2 + 6) * 5 * 9) + 4
5 + 3 + ((4 + 9 + 8 * 4 * 3) * 4 + 3 * 6)
5 + ((4 + 4) * 2) * (9 * 5 + 3 * 3 + 9) * 7
9 * 2 * 4 * (4 + 3 * (4 + 3) * 3) * 4
3 + 4 * ((5 * 8 * 2 * 3) + (9 + 8 * 2) * (5 * 3 + 3 + 7) * (2 + 8) * (6 * 4 * 9 * 4))
9 + 3 + 5 + (4 + (3 * 2 * 6) * 9 * 9)
9 * 2 + 3 + 5 * 2 + 3
((9 + 3 + 3 * 6 * 4 + 5) + (9 * 3) * 4 + 2 + (2 * 5 + 5)) * (9 + 5 * 3 * 7) * 8 * 4 + 9 + (2 * 8 * 4)
9 + ((4 * 8 + 2) * (8 + 4 * 4) + 4 + (7 + 9 + 6 * 3 * 4 * 8) * 8 * (5 + 8 + 9 * 8)) + 3 * (4 + 3 * 5 + (3 * 4 * 9))
(9 * 3 + 9 * 8 + 6 + 2) + 6 * (4 + 4)
(6 + 3 + 6 + 4) * 9 * 5
6 + 8 * (6 + 3) * (7 + 6 + 7 * 9 * (2 + 5 * 2)) + 3
3 + 5 + (4 * 5 * 2) + 2 + 7
6 + 9 * 5 + (4 + 2 + 5) * (2 + 6 * 2 * 3)
8 * ((9 * 5 + 9 * 2 * 9 * 9) * (2 + 5 * 3 + 9 + 6 + 2) + 6 * 8 + (8 * 8)) * 6 + 6 + (9 * 8 * 8 * 2 * 5) + 5
3 + 5 + 2 + (2 * 4)
2 * (2 + 6 + 8 * 2 + (2 * 5 + 6) * 5)
(9 + (2 * 8 * 3 * 3) + 9 + 8 * 2) + 2 * 9 + 7
2 * (9 + 6 + 7) + (2 * 2 * (7 + 8) * 4 * (4 * 4 + 3 * 5)) + 6 * 3
(3 * 4 * (5 + 9) * (7 * 3 * 3 + 3 + 9 + 8)) * 2 * 8
(7 * 2 + 7) * ((2 * 4 * 5) + 2 * 3 + (7 + 8)) + 3 + 9 + 3
4 + 2 * (5 * 4 + 7 + 6) * 6
2 * (7 * 3)
((9 + 9 * 4 * 4 + 6) + 6 + (4 + 7 * 9 * 5 + 9)) + 8 + 7
3 + (6 * 3 * (8 + 4 * 8 + 8 * 7 + 9)) * 3 + 5 + (5 + 4 * 7 + 4 + 3 * 4) * 2
5 * 5 * 3 * 9 + ((5 + 2 + 8) * 2 * 2)
(9 + 5 * 7 + 8 * 5 * (2 * 6 * 8 + 3)) * 5
5 * 7 * 4 * ((6 + 5) * 3)
5 * 8 * ((3 + 2 + 8) + 2 + (7 * 2) + 3 * 9 * 9) + 5 * 8 + (5 + 8 + 2 * 6)
2 + 2 * ((4 + 6 * 9) + 3 + 5 + 8)
(5 * 6 + 8 + 5) + 7 * 5 * 8 * 2
2 + (7 * 2 * (2 * 9) * 3) * (5 + 7) * 8
((8 * 3 + 2 * 8) + 4) + 2 + 8 + 3 * 7 + 9
2 + 3 + 3 * 9 + (4 + 6 + (6 + 8) + 5 * 5 * 5)
9 * 8 * (2 * 3 + (4 * 8 * 7 + 9) * 7 + (6 + 7 + 9)) * 7
6 * (8 * (4 * 3 + 9 + 7 + 2) * 8 + (6 + 4 + 9 + 7) + 3)
(5 * 8 * (2 * 8) + 7 * 4) + 3 + (7 + 6) * 3
(5 + 4) * 3 + ((9 * 2) + 3 + 5) * 2
(2 * 6 * (8 * 5) + 9 * 5) + (3 * 9 + (2 + 5 + 6))
6 * (5 + 3 * 3 * (2 * 6 * 4 + 6 * 4 + 6) + 4) + 5 + 2 * 4
(9 + 3 * 8 + 8) + 3 + 3 + 9
3 * ((8 * 4 + 4 + 9) * 7) + 8 * 3 * 7
5 * 2 + 9 * 7 + 4
5 * 5 + 7 * 6
2 * 7 + ((2 * 8 + 4 + 4 + 2 + 4) * 9 * 2 * (9 + 7 + 8 * 9)) * 5 + 8
4 + 6 + ((2 * 3) + 2) * 4 + 9
2 * ((6 * 6 * 5 * 6 * 3 * 9) * 6) * 4
((4 * 8) * 3 * (9 * 2 * 5 * 3 * 3 * 2) + (6 * 7 + 3 + 9) + (3 + 5) * (6 * 4)) * 4 * 8
5 * 3 + (9 + 4) + 3 + 9 * 6
(5 * 8 + 5 * 2 + (7 * 5 + 4)) + 5 * 9 * 7
(6 * 2 + 9 * 5 * 5) + 2 + 2 * 5
3 * (4 + 7 + 2 * (7 * 9 + 5 * 6 * 6)) * 6 + 8
6 + 9 + (4 * 4 + 6 * 7 + 6) * 5 + 8 + 6
8 + ((2 + 5 + 4) * 9 * 5)
4 * (9 + 5 + 7 * (6 + 8 * 7 * 2) * 4 + (9 * 8 * 7))
5 * 5 + (9 * 4 * 5) + 2
(4 + 3 * (5 + 9 + 8 + 4) + 4 * 4) + 2 * 2 * (5 * (3 + 9) * (4 * 9 * 8) * (2 + 8 + 3 + 5 * 4 + 4) * 6)
7 * 2 * 3 + 2 + 9
(9 + 5 * (8 * 2 * 5) * 9) + 7
8 + 3 + (8 * 2 * 6 * (8 * 2) * 4 + 3) * 2
(3 + 6 * 3) + 9 * (7 * 6)
5 + (6 * 3 + 6 * 3 * 7) + 4 + 9
9 + (7 * 5 * 8 * (8 * 2 * 5 + 5 * 3) * 3 * 5) * 2 * 4
4 * ((6 + 7 * 9) * (9 * 2 + 2)) + ((2 + 3 + 5 + 6 + 9) * 9) + 6
9 * 3 + (4 * 7 + (9 + 8 + 5)) + 4 + 4
((3 * 4 + 6) * 9 + 8) * 4 * 3 * 2 + 7
(6 * 9 + 3 * 5 + 2) + 2 * 5 + (8 * 8 * 7 + 6) + ((6 * 5 + 4 + 3 * 9 + 7) + 3 + 4 * 3 * 9) + 5
7 * (8 + (7 * 2 * 7 + 5 * 4) + 7 + 4 + 7 + 2) * 2 * 4
3 + (7 * 8 + (3 * 9 * 9 * 6 * 6)) + ((7 * 8 + 8 * 9) + 5 + 2 * 9 * 6 + 8) + 3 * 6 + (8 * 8 + 2 * 8)
4 * 4 * 5
(3 + 8) + 4 + 6 * 8 * 5
4 * (8 + 9 * 2 * (5 * 9) + (2 + 8)) * 5 + 8
(3 * (6 * 4 + 9 + 8 * 5 * 3) * (7 + 4) + (7 * 2 + 4 * 7 + 5) + 7 * 4) + 9 + 2 * 5 * 8
3 * 8 + 6 + 6 * 4 * 9
4 * 5 * 2 * (2 * 5 * 4 + 5 * 4 * 2)
((3 * 9 * 9) + 4 + 7 * 4 * (8 + 2 * 4 + 3 + 3) + 8) * 8 * (6 * (5 * 8 + 9 + 9 + 3 * 8) * 7 * 5 * 2)
(5 * 3 + (6 * 6 + 3 + 7 * 6 * 9)) * 9 * 2 * 5 + 3
9 + (7 * (7 + 9) + 3 * 2)
2 + 5 * ((3 + 4 + 5) * 6) * 9 + ((6 * 6) + 5)
5 + (5 + 2) + (3 + 8 + 4) + 6 * 4 + 8
(6 + 5 + 7 + 3) * 5 + ((6 + 7 + 5 + 3) + (2 * 6 + 9 + 2 * 8) + 4) * ((7 * 7 + 8 + 8) + 4 * 7 * 8 * 2 + (4 + 5)) + (7 + (5 * 9 * 6 * 5) * 9 * (7 + 2) + 7 * (8 * 6 + 9))
(9 * 8 * 5 + 8 + 9 + (6 * 9 + 4)) + (6 * 4 * 8 + 3 + 4 * 5) + 8 + 5
5 * 2 * (4 + 5 * 6) + 5 + 9 + (9 * 9 + 2 + 3 + (3 + 2 * 4 * 6 * 3))
3 + 2 + (7 * 9 + (4 + 8 * 2)) + 3 * ((7 + 9) * 9 + 8 * 8)
(5 * 7 * 4 + 3 * 2) + 9
7 * (5 + (7 * 4 + 6 + 7)) + 2 + 3
(4 + 4 * 7) * 2 * (7 + (8 + 8 * 8 + 2 + 7) * 2)
(6 + (4 * 2)) + 5 * 8
6 + (5 + 2 * 7 + 4 * (8 * 7 + 3 + 3 + 9 * 9)) * 6
4 * 9 * (9 * 8 * 4 + 2 + (5 * 6 + 7) * (7 + 5 * 7 * 4 + 8)) + 9 + 2 + (4 * 8)
((9 + 7 * 7 * 3 * 6) * (9 + 6 * 2 * 7 * 6 + 6) + 2 + 3 * (8 * 8 * 5) + 8) + 6 + 5 * 4
(6 + (5 * 2) + 9 + 6) + 2 * 8 * 8
8 * 2 * 5 * 8 * 4 + ((2 + 6 * 6 + 4 + 4) + 7 + 6 * 5 + 6 * 5)
4 * 5 * ((8 * 5 * 6) + 2 + (5 + 8 + 3 * 9 * 9) + 8 * 2)
2 * ((2 * 9 * 8 * 7) + (2 + 2 * 6 + 6) * 2)
9 + 3
3 * (5 + 2 + 6 + 5 * 6 + (3 + 7 + 4 + 7 * 8 * 5)) * 9 + 7 + 7
2 + (7 * 5 * (4 * 5 * 5)) + (7 * 7 * 3 * 3 * 7)
8 + (4 + (6 + 3 * 9 + 2 + 8) * 6 * 5) + 3 + (3 + 8 * 3 * 3 + (7 + 9 * 3) + 4)
((9 * 4 + 5 * 2 * 4) * 4 + 8 * (9 * 8 + 7 * 7) * 9) * 8 + 4 * 2 * 2 + 4
3 * 6 + (4 * 7 + (4 + 8 + 7 + 7) * (9 * 4) + 4) + 3 * 7 + 7
7 * (8 + 8 + 2 + 4 + 6) + 7 + (8 + 3) + 3
(3 + (9 + 3 + 8 * 8 + 2 + 2)) + 2 + 9 * 2 + 5 + 4
5 * 3 * 8 + 7 + (3 + 4 * 9 + 7 * 7)
3 * 2 * (2 + (3 * 5 + 3 * 3) * (9 + 6) + (5 + 5 + 5 * 4)) * 4
(3 * 5 * 9 + 4) + 8 * (5 + 7 + 6 + 4)
2 * 9
8 + 2 + 5 + ((8 + 3) * 4 + 6) * 4 * 6
8 + (5 * 7 * 2 + (6 * 6 * 2 * 5) * 5 * 5) + (5 + 3 + (7 * 5) * 5) * (5 * 7 * 6 + 3 * 2 + 3) + 7 + 5
2 + (5 * 8 + 9) * 7 * 7 + 8
(6 + 6 + (6 * 7 * 6 + 4 + 2)) + 8 * 5 + 9 + 7 * 5
6 + (3 * 9) + 9
6 * (5 + 6 * 9 * 8 * 5 * 6)
6 * 4 * (4 * (7 + 4 * 9 + 4 * 3) * 4 + 5) + (4 + 6 * (5 + 4 * 9) * (8 + 7 * 8 + 9 * 2))
4 + 7
((5 + 4 * 3 * 7 + 2 * 3) * (8 + 5 * 4 + 3 + 6) + 7 + 9 * 2 * 9) * 7 + 6 + 7 + 7
(5 * 9 + 2 * 6 * 4) + 8 + 7 * 3 * 2
2 * (8 * (2 * 2 + 6)) * 3
9 + (9 * 7 + 3 + 5 + 2 + 3) * 2 * 2
(8 + 4 * (2 * 5 * 6) + 9) * 8 * 6
7 * 2 * (6 + 4 + 3 + 9) * 7 + 4
2 * 3 + 8 * ((5 + 9 + 6) * 5 * (7 * 4 * 5)) * 8
3 + ((2 + 3 * 2 * 9 * 3 * 4) + 6 * 5 * 2 * 2)
7 * ((5 + 7 * 9 + 9 + 3 + 2) * 5 * 6 * 8 + 6 + (9 + 9 + 7 * 8 * 2 + 3)) * 3 * 4 * 6
3 + 3 + (6 + 6 * 6 + 7 * 9 * 4) * 8
3 + 4 * (4 + 5 + 8 * 4) * (3 + 7 * 6 * 7 + (9 + 4)) * (2 + 8 + 5) + 6
5 * 9 + (4 * 6 + 7 * 5 + 8) + (2 + 4) * 4
(6 * 2 * 7 * 8 * 9 + 7) * 9
9 + (5 + 2 + 8 + (6 * 9 * 7 + 2)) + 9
7 + (7 * 9 + 8 * 5 + 2 * 2) + 8 + 6 * 5
(6 + 6 * 8) + (5 * 5 + 6 + (9 * 2 * 6 * 3) + 2 * 7) + 8
(2 * (5 * 8 + 5 + 5) + 7) + 5 * 8 * 6 * 8 + 2
(8 * (7 * 7 * 3 * 4 + 9 * 4) * (9 * 3 + 8) + 6) * 7 * 6 * 2 * 8
9 * ((9 + 7 + 3 * 5 * 3 * 5) + 6 * 7 * 3) * (6 + 6 + 6 * 2 + 9 + 5) + 4 * 9
4 * 3 * 8 + ((9 + 3 * 2) * 9 + 2 + 2)
3 + 6 * ((6 * 2) + 2 * 3 + 6 * 5)
7 * (2 * 5 * 2) * 5
(8 + 2) + (6 + 9) * 6
((2 * 3) * (8 * 7 * 4) * 4 + 8) * 9 * 9 + (7 * (4 * 2 + 2 * 7) + 4) * (9 + 3 * 6 + 6 + 8 + 7)
5 * 5 + (8 + 7 + 4 * 2) * 7 + 9 * 2
5 + (2 * 7 * 3) * 9 * 9
3 + (3 * (6 * 2)) + 6 + (3 + (7 * 7 * 6 * 3 + 4) * 9 + (9 + 9 * 5 * 2 + 9 * 4) * 6 * 6) + 9 * 6
2 + 9 * (6 * 6 * 3 * 4)
8 * 6 + (3 * 9 * 9 * 4 * (8 * 2 * 2)) + 8 + 5
(2 + (7 + 3 + 2 * 6 * 8)) + 4 * ((5 + 4 * 7 * 2) * 8 * 5 + (5 + 8 + 9 * 9)) + 8 + 6 * (8 + 7 * (7 + 3 * 4 * 7) * 4 + (9 * 8 + 3) + 3)
(4 * (8 * 8 * 6 * 2) * 7 + (8 + 3) + 7 + (6 * 4)) * 3 * 8 * 2 * 9
3 + (4 * 3 + 3 * 2) * 7 * 3
(4 + 2 + 8) * 2 * 6 + 8 + 9
7 * (8 * (8 * 7 * 6 * 5) + 8 * 6 + 6)
(7 + (6 + 3 * 5) + 2 + 4 * (5 + 9 * 7 * 5 + 7) * (2 * 4 + 6 * 5 + 2 * 3)) + 9 * 7
5 * (5 * 5 * 4 * 2) + 3
(5 + 8 * 8) * (5 + 5 * 7) * 3 * 5
8 + (7 + 6 * 3 * 4) + 5 * 4 * 3 + 8
3 * 5 * ((4 + 2 + 5) * 8 * 3 * 4 + 5) + 4 + 9 + 4
(9 * (7 + 2 + 6 + 4 + 4 * 2) * 8 + 6 * 8) + 3 + 9 * 3
9 + 8
6 + 6 * (7 + 9 + 7 + 3 + 4) + 6 + 7
7 + (6 + 2 + 6 + 7)
6 + (9 * (6 + 2 + 6 * 9 * 2) * 7 * (9 + 6 * 7 * 3 + 5 + 2))
3 * 7 + 2 * 9
9 * 7 + 9 * (2 * (2 * 6 + 6 * 6) + 8 * 9 * 2)
((7 * 9 + 7 + 7) * 6 * 5) + 4
2 * (7 + 4 + 3 * 5 * 6 + 3) * 5 * ((6 * 6 * 9 + 3 + 2 + 9) + 5 * 3 + 3 + 5 * 3) * 7 + (8 + 4)
(7 * 6 * 9 * 3 + 8 + 4) + 6 * 7 * 6 + 3 + 4
2 + 6 * 7 * 5 + (7 * (2 * 3 + 4 + 6) + (3 * 3 + 7 * 4) * (3 + 8 + 7 + 6 + 4 + 4))
(8 * 3 + (7 + 4 + 5 + 5)) + (3 + 3 + 7 * (9 + 2 * 9))
8 * 4 + (9 + (7 * 3 * 9 + 4 * 7) + 6 + 9 + 5 + 6)
2 + 4 + 2 + 4 * 7
((7 + 5 + 5 + 3) * 3) + (5 + 6 + (3 + 8) * 2 + 4) + (6 + 2 * (5 + 6 + 9 + 8 + 5) + (9 * 9) + 3 * 6) * 3
(5 * 7) * 6 + (8 + 4) * 6
(2 * 4 * (8 * 6) * 2) + 5 + 8 * 9 * 7
((5 + 5) * 5) * 9 + (5 + 4) + 4 * 6
5 * (8 + (4 * 5 + 7 + 5) + (8 + 4 + 6)) + 9 + 7
2 * 8 + 7 + (3 + (8 + 8 * 5 * 5 * 8 * 5) + 9 + 9) + 7 + (7 * 8 + (3 + 5 + 8 * 7 * 8 * 8))
(9 + 7 * (7 + 6 + 8 + 2 * 5 * 2)) * 9
4 + (2 * 5) * (4 * (8 + 5 * 4) * 9 * 4 + 9)
4 + (8 + 8 + 8) + 3 * 2 + (2 + (5 * 6 * 7)) + 7
((5 * 3 * 6 * 6 + 7) + 2 + 2 + 2 * 8 + (6 + 3 + 2 * 4 + 8)) + 4 + 3 * (4 + 6) * 9 * 3
5 + 9 + 7 + 4 * ((2 + 5) * 4 + 9 * 9 + 4)
6 * 7 + 4 * 5 + (4 * 7 * 8 * (2 * 4 * 9 + 9 + 5) * 6)
6 * 9 + (3 * 9) * 8 + (4 * (8 * 3 * 2 * 2 + 3) * 4 * 8 * 7 * (4 + 5 + 7 * 3 + 4 * 3))
8 + (4 + 2) * (8 * 8 + 9 + 5)
((6 * 8 + 3 * 2 * 6 * 9) + 7 + 8 + 4) * 4
4 + (2 * 3 + 3 * 7) + 2 * (2 * 5 + 7 * 6) + 8 + 6
8 + 7 + ((3 + 2 + 4 * 5 * 3 * 2) + (2 + 4) * 8 + 5 * (7 * 2 * 9) + 3) * 2
8 + 6 + (7 + 6 * 7 * 8 + 4)
(7 * 9 + (2 + 6)) + 5 + 3 * 8 + 7 * 3
(6 * 9 + 6 + 3 + 5 + 4) * 8 * 4 * ((8 * 4 + 7) + 2 + 9 + (3 + 4 + 8 + 6 * 2 + 7) + (3 + 2 + 7 + 5 * 7) + 2)
8 * 8 + 2 * (8 * (8 + 6 + 6 + 5 * 6)) + (2 + 6 * 3 * 5)
8 * 3 * 3 + (3 * (4 + 6 + 6 * 4 * 2) + 4 + (7 + 3 * 3 * 9) + 6)
5 + 7 + (3 * 8 + 8 + 2 + 4 + 9)
(3 * (5 * 3 + 2 * 8) + 8) * 7 + 5 + (8 * 5 + 3 * (6 * 9 * 5 + 9 * 6)) + 4
6 + (6 * (3 * 2 * 4 * 7 + 4 + 5) * 4) * 5 + 8 + 3 + 5
(8 * (2 + 8) + 9 + 7 + 8 * (3 * 6 + 7 * 6 + 5 + 7)) * (2 + 9 * (4 * 7 + 3 + 4 + 6 * 5)) * ((2 + 9 + 6 * 3 * 8) * 5 * 4) * 7
5 * 7 + 2 + 7 * (5 * 8 * 5 * 4 * 2 + 3) + 8
2 * 6 + 2 + 8 * 6 * 6
4 * 4 * (9 * 8)
8 * (2 * 8) + 5 * 5
2 + 4 * 3 * 8
8 * (5 + (8 + 9 + 8) * 8) * ((3 + 9 + 8 + 2 + 7 * 2) * 8 * 5 + 8) * 7 + 2
(2 * 3) + 3
4 + 4 + 2 + 5 + ((9 + 2 * 2 + 6 + 6) * 4)
7 * 9 + (9 + 4 * (4 * 2 * 7 * 6 * 2 + 4))
8 + (6 + 8) + 3 + 2 + 3
(3 * 4) + 8 + 3 + (3 + 5 * 3 * (4 + 8 * 7 + 2) * (4 + 7) * 5) * 3 + 2
3 + (6 * 3 + 2) + 6 * (6 + 4 * 8) + 7
6 * 7 * 9 + 8 * 6 * (4 * 4 + 8 + 3 + 7)
8 * 9 + 9 + (7 + 5) + 8 * 6
(4 * 3 + 2 * 7) * 3 * 4 * 6 * 2 + 5
(8 * (4 + 9 * 5) * 5 * (4 * 9 + 8 + 6 * 7)) + 6
6 + 8 * 9 * (9 + (4 * 6 + 2) + 8 + 8 + 8)
4 + 4 * ((3 + 4 + 5) * (3 * 6 + 2) * 4 + 4 + 9) + 4 + 7
3 * 3 * 4 * (6 * 6 + 4 + 2 * (7 * 5 * 7)) * 3 * (4 + 4 * 3 * 4)
2 + (5 * 9 * 2 * 8 * 4 * 6) + 8
6 + (6 * 2 * 9 * 4 + (5 + 8 * 7) + 4) + (5 + 9 * 5 * 7 + (5 * 9 + 6)) + 3 * 3 + 8
2 + (4 + (9 * 6 + 2 * 6) * 2 + 7 * (4 * 4)) * 7 + 2 + 3 + 7
(4 + 4 + 8 * 9 * 2) * (9 + 5 + (6 + 4) * 4 + (6 + 3 + 7 * 9 * 3) * 7)
8 * 9 * (7 + (7 * 4 + 7 * 2 + 7)) * 9
(9 * 6 * 8 * 6) + (3 * 4 * 5 * 8) * 6 + (9 * 5 + 6 + (3 * 2) + (5 + 8 * 5 + 6 * 9 + 5)) * 7
7 + 3 * 2 + (5 + (5 + 8 * 9 * 3 + 3) * 9 * 9) + 2
7 + 7 * 4 * (8 * (9 * 8 * 9 * 8 + 3) + 6 * (2 + 7 * 8 + 8) * 3 * 2)
8 + ((4 + 2 * 4 * 7 * 2) + 2 * 9 + 9 + 7) * 9 + 9 * 3
(8 + (5 + 5 + 2) * 8 + (8 + 6 + 4 * 7 * 2)) + 2 + 9
(6 * 3 + (9 + 6) + 8 + 3) * 5 + 6 * 7 + (5 * 9) + 9
2 + ((3 * 2 + 6) * (6 * 5) + 6) + 6 + 4
6 + 6 * 7 * (3 * 8 + 8) + 2
((8 * 9) + 3 * 7 * 4) + (3 * 7 * 5 * 6)
(3 * (8 + 6 + 3 * 9 * 3 * 4) * 9) + 2
((7 * 9 + 7 * 2) * 2 * 3 + 6) * 5 * 4 * 9 + 5
5 + (5 + 6 + 7 + 2 + 6 + 6) * 9 * (5 + 4 + 3) * 6 * 9
(9 * 9 + 8 * 5 + 4 * 5) + 9 * (3 + 3 + 8) + 2
6 + 5 * (5 * 9 + (6 + 4 * 5 + 2 + 7 * 6) + 5 + (9 * 9)) * 4 * 5
(2 + 2 * 7 + 6 + (5 + 3)) * 7 * 4
((4 * 3) * 8 * 2 * 7 * 3 * 2) * 4 * 2 + 8 * (5 + 6 + (9 * 6 + 2 * 4) + 4 + 7 + 9) * 8
3 + 4 + 7 + (2 * 7 + (2 + 9 + 3 + 6) + (2 * 2 + 4 + 3 + 2 * 5) + (3 * 8 + 4 + 4 * 9 * 4)) * 5
8 * 6 * 2 * ((6 * 7 * 4) + 6 * 9)
5 * 5 * 6 * (7 + (4 + 2 * 6 * 6)) + 9 + 7
4 + (5 + (2 * 3) * 8 * 3 + 8 + 5)
(5 * 9 * (3 * 6 * 9 * 4) + 4) * 9 + 5
(5 * (5 + 3 + 8 + 9) * 3) + 6
(7 + 9 + 3 + (7 + 3 + 5 * 2) + 9 * 5) + 8 + 8 + (3 + 8 * 9 * 9 + (7 * 7 + 2 + 5 * 6 + 4) * 7)
((9 * 9 * 3 * 4 + 9) * 5) * 7
4 + 3 + 7 * 6 * (6 + 2 * 6 + 9) * 6
8 * 2 + 7 + (9 + (6 + 7 * 8 + 5 + 2) * 3 + 4 + (8 * 4)) * 2 + 6
3 + 5 * 6 * 2 + (9 + 3)
(7 * 2 + 4 * 3 + 2 * (6 + 4 * 8 * 2 * 9 + 9)) * 7
(8 + 9) * 8 + 7 + 5
2 * 6 * 7 + (5 * 9) * 5
(5 + (4 + 7 * 9 * 3) * 8 + 3 * 2 + 3) + 8 * 9 + (4 + 3 * (2 + 4 + 4) + (3 * 3 + 8 * 6) + 5 * 8) * 4
9 * 8 + (6 * 5) * (6 + 7)
(3 * (2 * 4 + 9 * 5) + 2 * 5) * ((5 + 9) * 2 + 2)
2 * (2 + (2 * 9 + 7 * 6 + 8))
(5 + 2 + 6 + 2 + 6) + (2 * 6 + 4 * 3 + 9 * 4) * (2 + 9 * 7 + 6) + (5 + 5) * (2 + 5 + 7) * 6
5 + 4 + 3 + 2 + (3 + 4 + 3)
((6 * 9 + 7 + 7) * 9 * 7 + 7 * 6 + 6) * 2 * 9 + ((5 * 6) * 3 * 3 * 8)
5 * 8 + (7 + 2 + 6) * (8 + 2 + 8 + 4 + 9) + (5 * 6 * 6 * 3) + 8
6 + (7 * 4 + 4 * 7 + 3 * 7) + 9 + 5 * 7 * 5
3 + 2 + 8 + (4 + 9 * 4 * 2 + (9 * 9 + 9 + 5) * 5) + 6
3 * 9 * 4 * (5 * 4 * (8 + 3))
";

    }
}