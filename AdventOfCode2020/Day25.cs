﻿using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture(TestName = "Day 25: Combo Breaker")]
    public static class Day25
    {
        // p*q1 = pubKey1
        private static long BruteForceExponent(in long pubKey)
        {
            var result = 1L;
            var q = 0L;
            do
            {
                q++;
                result = (7 * result) % 20201227L;
            } while (result != pubKey);
            return q;
        }

        private static long GetEncryptionKey(in long q1, in long pubKey2)
        {
            var result = 1L;
            for (var q = 0; q < q1; q++)
                result = (pubKey2 * result) % 20201227;
            return result;
        }

        [TestCase(5764801, 17807724, ExpectedResult = 14897079)]
        [TestCase(6930903, 19716708, ExpectedResult = 10548634)]
        public static long Part1(in int pubKey1, in int pubKey2)
            => GetEncryptionKey(BruteForceExponent(pubKey1), pubKey2);
    }
}