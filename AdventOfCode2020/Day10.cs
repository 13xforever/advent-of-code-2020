﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture(TestName = "Day 10: Adapter Array")]
    public static class Day10
    {
        private static int[] BuildLongestAdapterChain(this IEnumerable<int> ratings)
        {
            ratings = ratings.OrderBy(r => r);
            var result = new int[4];
            var previous = 0;
            foreach (var adapter in ratings)
            {
                var diff = adapter-previous;
                Assert.That(diff, Is.LessThan(4));
                result[diff]++;
                previous = adapter;
            }
            result[3]++;
            return result;
        }

        private static int GetDiffRating(this in Span<int> ratingDiffs)
            => ratingDiffs[3] * ratingDiffs[1];

        private static long GetPermutationCount(Span<int> ratings)
        {
            if (ratings.Length < 3)
                return 1;
            
            var result = 0L;
            var startRating = ratings[0];
            do
            {
                ratings = ratings[1..];
                if (ratings[0] - startRating < 4)
                {
                    var subPathCount = GetPermutationCount(ratings);
                    result += subPathCount;
                }
                else
                    break;
            } while (ratings.Length > 1);
            return result;
        }
        
        private static long GetChainCount(this IEnumerable<int> ratings)
        {
            var list = ratings.Concat(new[]{0}).OrderBy(v => v).ToArray().AsSpan();
            long result = 1;
            var previousIdx = 0;
            for (var i = 1; i < list.Length; i++)
            {
                var isLast = i == list.Length-1;
                if (list[i] - list[i - 1] == 3 || isLast)
                {
                    var span = isLast ? list[previousIdx..] : list[previousIdx..i];
                    var permCount = GetPermutationCount(span);
                    Console.WriteLine(string.Join(" ", span.ToArray()) + " => " + permCount);
                    result *= permCount;
                    previousIdx = i;
                }
            }
            return result;
        }
        
        [TestCase(TestInput1, ExpectedResult = 7*5)]
        [TestCase(TestInput2, ExpectedResult = 22*10)]
        [TestCase(RealInput, ExpectedResult = 2368)]
        public static int Part1(in string input)
            => input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .BuildLongestAdapterChain()
                .AsSpan()
                .GetDiffRating();

        [TestCase(TestInput1, ExpectedResult = 8)]
        [TestCase(TestInput2, ExpectedResult = 19208)]
        [TestCase(RealInput, ExpectedResult = 1727094849536)]
        public static long Part2(in string input)
            => input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .ToArray()
                .GetChainCount();
        
        private const string TestInput1 = @"
16
10
15
5
1
11
7
19
6
12
4
";
        private const string TestInput2 = @"
28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3
";

        private const string RealInput = @"
30
73
84
136
132
117
65
161
49
68
139
46
21
127
109
153
163
160
18
22
131
146
62
113
172
150
171
98
93
130
170
59
1
110
2
55
37
44
148
102
40
28
35
43
56
169
33
5
141
83
15
105
142
36
116
11
45
82
10
17
159
140
12
108
29
72
121
52
91
166
88
97
118
99
124
149
16
9
143
104
57
79
123
58
96
24
162
23
92
69
147
156
25
133
34
8
85
76
103
122
";

    }
}