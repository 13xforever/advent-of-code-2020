﻿using System;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture(TestName = "Day 23: Crab Cups")]
    public class Day23
    {
        const byte Len = 9;
        const int BigLen = 1_000_000;

        private static string Shuffle(in ReadOnlySpan<char> input, in int moves)
        {
            Span<byte> circle = stackalloc byte[Len];
            Span<byte> buf = stackalloc byte[3];
            for (var i = 0; i < Len; i++)
                circle[i] = (byte)(input[i] - '1');

            var currentIdx = 0;
            for (var round = 0; round < moves; round++)
            {
                var dest = (byte)((circle[currentIdx] + Len - 1) % Len);
                for (var i = 0; i < 3; i++)
                {
                    buf[i] = circle[(currentIdx + 1 + i) % Len];
                    if (buf[i] == dest)
                        dest = (byte)((dest + Len - 1) % Len);
                }
                while (buf.IndexOf(dest) > -1)
                    dest = (byte)((dest + Len - 1) % Len);
                var destIdx = circle.IndexOf(dest);
                for (var i = (currentIdx + 4) % Len; i != (destIdx + 1) % Len; i = (i + 1) % Len)
                    circle[(i + Len - 3) % Len] = circle[i];
                currentIdx = (currentIdx + 1) % Len;
                for (var i = 0; i < 3; i++)
                    circle[(destIdx + Len - 3 + 1 + i) % Len] = buf[i];
            }
            Span<char> resultBuf = stackalloc char[Len - 1];
            var seqIdx = circle.IndexOf((byte)0);
            for (var i = 0; i < Len - 1; i++)
                resultBuf[i] = (char)(circle[(seqIdx + i + 1) % Len] + '1');
            return new(resultBuf);
        }

        private static long ShuffleBig(in ReadOnlySpan<char> input)
        {
            const int moves = 10_000_000;
            
            var circle = new int[BigLen];
            Span<int> buf = stackalloc int[3];

            for (var i = 0; i < Len; i++)
            {
                var v = input[i] - '1';
                var nextV = input[(i + 1) % Len] - '1';
                circle[v] = nextV;
            }
            circle[input[^1] - '1'] = Len;
            for (int i = Len; i < BigLen; i++)
                circle[i] = i + 1;
            circle[^1] = input[0] - '1';

            var current = circle[^1];
            for (var round = 0; round < moves; round++)
            {
                var dest = (current + BigLen - 1) % BigLen;
                ref var tmp = ref circle[current];
                // copy next 3 items into a buffer for quick check
                for (var i = 0; i < 3; i++)
                {
                    buf[i] = tmp;
                    tmp = ref circle[tmp];
                }
                // cut nodes off 
                var cutStart = circle[current];
                ref var cutEnd = ref circle[circle[cutStart]];
                circle[current] = tmp;

                // find destination item
                while (buf[0] == dest || buf[1] == dest || buf[2] == dest)
                    dest = (dest + BigLen - 1) % BigLen;
                var insertEnd = circle[dest];
                
                // reinsert cut nodes back
                circle[dest] = cutStart;
                circle[cutEnd] = insertEnd;

                // move current item pointer
                current = circle[current];
            }
            ref var seqIdx = ref circle[0];
            return (seqIdx + 1L) * (circle[seqIdx] + 1L);
        }

        [TestCase("389125467", 10, ExpectedResult = "92658374")]
        [TestCase("853192647", 100, ExpectedResult = "97624853")]
        public static string Part1(in string input, in int moves)
            => Shuffle(input, moves);

        [Parallelizable(ParallelScope.All)]
        [TestCase("389125467", ExpectedResult = 934001L*159792L)]
        [TestCase("853192647", ExpectedResult = 664642452305)]
        public static long Part2(in string input)
            => ShuffleBig(input);
    }
}