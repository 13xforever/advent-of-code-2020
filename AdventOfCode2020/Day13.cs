﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture(TestName = "Day 13: Shuttle Search")]
    public static class Day13
    {
        private static (int delay, int id) GetFastestDeparture(in string input)
        {
            var parts = input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
            var baseTime = int.Parse(parts[0]);
            var schedule = parts[1].Split(",")
                .Where(v => v != "x")
                .Select(int.Parse)
                .ToArray();
            var result = (delay: int.MaxValue, id: 0);
            foreach (var id in schedule)
            {
                var delay = (id - baseTime % id) % id;
                if (delay < result.delay)
                    result = (delay, id);
            }
            return result;
        }

        private static int MulInv(in long a, in long mod)
        {
            var b = a % mod;
            for (var x = 1; x < mod; x++)
                if ((b * x) % mod == 1)
                    return x;
            return 1;
        }

        private static long Crt(in ICollection<(int remainder, int mod)> list)
        {
            var commonMod = 1L;
            foreach (var (_, mod) in list)
                commonMod *= mod;
            var sm = 0L;
            foreach (var (remainder, mod) in list)
            {
                var p = commonMod / mod;
                sm += remainder * MulInv(p, mod) * p;
            }
            return sm % commonMod;
        }
        
        private static long GetPeriod(in string input)
        {
            var parts = input.Split(",");
            var cycles = new List<(int remainder, int id)>();
            for (var i = 0; i < parts.Length; i++)
            {
                if (parts[i] == "x")
                    continue;

                var id = int.Parse(parts[i]);
                cycles.Add((id - i % id, id));
            }
            return Crt(cycles);
        }

        [TestCase(TestInput, ExpectedResult = 5*59)]
        [TestCase(RealInput, ExpectedResult = 4938)]
        public static int Part1(in string input)
        {
            var (delay, id) = GetFastestDeparture(input);
            return delay * id;
        }

        [TestCase(TestInput, ExpectedResult = 1068781)]
        [TestCase("17,x,13,19", ExpectedResult = 3417)]
        [TestCase("67,7,59,61", ExpectedResult = 754018)]
        [TestCase("67,x,7,59,61", ExpectedResult = 779210)]
        [TestCase("67,7,x,59,61", ExpectedResult = 1261476)]
        [TestCase("1789,37,47,1889", ExpectedResult = 1202161486)]
        [TestCase(RealInput, ExpectedResult = 230903629977901)]
        public static long Part2(in string input)
            => GetPeriod(input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries)[^1]);

        private const string TestInput = @"
939
7,13,x,x,59,x,31,19
";

        private const string RealInput = @"
1008169
29,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,41,x,x,x,37,x,x,x,x,x,653,x,x,x,x,x,x,x,x,x,x,x,x,13,x,x,x,17,x,x,x,x,x,23,x,x,x,x,x,x,x,823,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,x,19
";

    }
}