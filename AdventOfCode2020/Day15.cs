﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture(TestName = "Day 15: Rambunctious Recitation")]
    public static class Day15
    {
        private static int GetSequenceNumber(this IEnumerable<int> seedSeq, in int idx)
        {
            var mem = new Dictionary<int, int>(3_612_000);
            var i = 1;
            foreach (var n in seedSeq)
                mem[n] = i++;
            var lastNum = 0;
            do
            {
                var numToUpdate = lastNum;
                if (mem.TryGetValue(numToUpdate, out var lastSpokenIdx))
                    lastNum = i - lastSpokenIdx;
                else
                    lastNum = 0;
                mem[numToUpdate] = i++;
            } while (i < idx);
            return lastNum;
        }
        
        private static int GetSequenceNumberUsingArray(this IEnumerable<int> seedSeq, in int idx)
        {
            var mem = new int[29_600_000];
            var i = 1;
            foreach (var n in seedSeq)
                mem[n] = i++;
            var lastNum = 0;
            do
            {
                var numToUpdate = lastNum;
                var lastSpokenIdx = mem[numToUpdate];
                if (lastSpokenIdx == 0)
                    lastNum = 0;
                else
                    lastNum = i - lastSpokenIdx;
                mem[numToUpdate] = i++;
            } while (i < idx);
            return lastNum;
        }

        [TestCase("0,3,6", ExpectedResult = 436)]
        [TestCase("1,3,2", ExpectedResult = 1)]
        [TestCase("2,1,3", ExpectedResult = 10)]
        [TestCase("1,2,3", ExpectedResult = 27)]
        [TestCase("2,3,1", ExpectedResult = 78)]
        [TestCase("3,2,1", ExpectedResult = 438)]
        [TestCase("3,1,2", ExpectedResult = 1836)]
        [TestCase("15,5,1,4,7,0", ExpectedResult = 1259)]
        public static int Part1(in string input)
            => input.Split(",", StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .GetSequenceNumber(2020);

        [Parallelizable(ParallelScope.All)]
        [TestCase("0,3,6", ExpectedResult = 175594)]
        [TestCase("1,3,2", ExpectedResult = 2578)]
        [TestCase("2,1,3", ExpectedResult = 3544142)]
        [TestCase("1,2,3", ExpectedResult = 261214)]
        [TestCase("2,3,1", ExpectedResult = 6895259)]
        [TestCase("3,2,1", ExpectedResult = 18)]
        [TestCase("3,1,2", ExpectedResult = 362)]
        [TestCase("15,5,1,4,7,0", ExpectedResult = 689)]
        public static int Part2(in string input)
            => input.Split(",", StringSplitOptions.RemoveEmptyEntries)
                .Select(int.Parse)
                .GetSequenceNumberUsingArray(30_000_000);

    }
}