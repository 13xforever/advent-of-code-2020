﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture(TestName = "Day 17: Conway Cubes")]
    public static class Day17
    {
        private sealed class Gol3dSim
        {
            private Gol3dSim() { }

            private readonly HashSet<(int x, int y, int z)> cells = new();

            public static Gol3dSim Parse(string input)
            {
                var result = new Gol3dSim();
                var lines = input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
                for (var y = 0; y < lines.Length; y++)
                for (var x = 0; x < lines[0].Length; x++)
                {
                    if (lines[y][x] == '#')
                        result.cells.Add((x, y, 0));
                }
                return result;
            }

            private byte GetNeighbourCount((int x, int y, int z) cell)
            {
                byte result = 0;
                var (x, y, z) = cell;
                for (var i = x - 1; i <= x + 1; i++)
                for (var j = y - 1; j <= y + 1; j++)
                for (var k = z - 1; k <= z + 1; k++)
                {
                    if (i == x && j == y && k == z)
                        continue;

                    if (cells.Contains((i, j, k)))
                        result++;
                }
                return result;
            }

            public Gol3dSim Step()
            {
                var result = new Gol3dSim();
                var checkedCells = new HashSet<(int x, int y, int z)>((int)Math.Min(cells.Count * 27L, int.MaxValue));
                foreach (var (x, y, z) in cells)
                    for (var i = x - 1; i <= x + 1; i++)
                    for (var j = y - 1; j <= y + 1; j++)
                    for (var k = z - 1; k <= z + 1; k++)
                    {
                        var newCell = (i, j, k);
                        if (!checkedCells.Add(newCell))
                            continue;

                        var count = GetNeighbourCount(newCell);
                        if (cells.Contains(newCell) && count is 2 or 3
                            || !cells.Contains(newCell) && count is 3)
                            result.cells.Add(newCell);
                    }
                return result;
            }

            public int Count => cells.Count;
        }

        private sealed class Gol4dSim
        {
            private Gol4dSim() { }

            private readonly HashSet<(int x, int y, int z, int w)> cells = new();

            public static Gol4dSim Parse(string input)
            {
                var result = new Gol4dSim();
                var lines = input.Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries);
                for (var y = 0; y < lines.Length; y++)
                for (var x = 0; x < lines[0].Length; x++)
                {
                    if (lines[y][x] == '#')
                        result.cells.Add((x, y, 0, 0));
                }
                return result;
            }

            private byte GetNeighbourCount((int x, int y, int z, int w) cell)
            {
                byte result = 0;
                var (x, y, z, w) = cell;
                for (var i = x - 1; i <= x + 1; i++)
                for (var j = y - 1; j <= y + 1; j++)
                for (var k = z - 1; k <= z + 1; k++)
                for (var l = w - 1; l <= w + 1; l++)
                {
                    if (i == x && j == y && k == z && l == w)
                        continue;

                    if (cells.Contains((i, j, k, l)))
                        result++;
                }
                return result;
            }

            public Gol4dSim Step()
            {
                var result = new Gol4dSim();
                var checkedCells = new HashSet<(int x, int y, int z, int w)>((int)Math.Min(cells.Count * 27L, int.MaxValue));
                foreach (var (x, y, z, w) in cells)
                    for (var i = x - 1; i <= x + 1; i++)
                    for (var j = y - 1; j <= y + 1; j++)
                    for (var k = z - 1; k <= z + 1; k++)
                    for (var l = w - 1; l <= w + 1; l++)
                    {
                        var newCell = (i, j, k, l);
                        if (!checkedCells.Add(newCell))
                            continue;

                        var count = GetNeighbourCount(newCell);
                        if (cells.Contains(newCell) && count is 2 or 3
                            || !cells.Contains(newCell) && count is 3)
                            result.cells.Add(newCell);
                    }
                return result;
            }

            public int Count => cells.Count;
        }

        [TestCase(TestInput, ExpectedResult = 112)]
        [TestCase(RealInput, ExpectedResult = 353)]
        public static long Part1(in string input)
        {
            var sim = Gol3dSim.Parse(input);
            for (var i = 0; i < 6; i++)
                sim = sim.Step();
            return sim.Count;
        }

        [TestCase(TestInput, ExpectedResult = 848)]
        [TestCase(RealInput, ExpectedResult = 2472)]
        public static long Part2(in string input)
        {
            var sim = Gol4dSim.Parse(input);
            for (var i = 0; i < 6; i++)
                sim = sim.Step();
            return sim.Count;
        }

        private const string TestInput = @"
.#.
..#
###
";

        private const string RealInput = @"
#.#####.
#..##...
.##..#..
#.##.###
.#.#.#..
#.##..#.
#####..#
..#.#.##
";
    }
}