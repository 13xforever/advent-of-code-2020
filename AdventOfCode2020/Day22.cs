﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace AdventOfCode2020
{
    [TestFixture(TestName = "Day 22: Crab Combat")]
    public class Day22
    {
        private sealed class Hand : Queue<byte>
        {
            public Hand(IEnumerable<byte> sequence): base(sequence){}
            
            public override bool Equals(object? obj) => ReferenceEquals(this, obj) || obj is Hand other && Equals(other);
            private bool Equals(Hand other) => this.SequenceEqual(other);
            public override int GetHashCode() => this.Aggregate(0, HashCode.Combine);
        }
        
        private sealed class Game
        {
            private Game(){}

            private Hand hand1 = null!;
            private Hand hand2 = null!;
            private Hand? winner;

            private readonly HashSet<int> handsState = new();

            public static Game Parse(in string input)
            {
                var parts = input.Split(Environment.NewLine + Environment.NewLine);
                return new()
                {
                    hand1 = new(parts[0].Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries)
                        .Skip(1)
                        .Select(byte.Parse)),
                    hand2 = new(parts[1].Split(Environment.NewLine, StringSplitOptions.RemoveEmptyEntries)
                        .Skip(1)
                        .Select(byte.Parse))
                };
            }

            private Game Clone(int h1Size, int h2Size) => new() {hand1 = new(hand1.Take(h1Size)), hand2 = new(hand2.Take(h2Size))};

            public void PlayNormalGame()
            {
                while (InProgress)
                    PlayNormalRound();
            }
            
            private void PlayNormalRound()
            {
                var v1 = hand1.Dequeue();
                var v2 = hand2.Dequeue();
                if (v1 > v2)
                {
                    hand1.Enqueue(v1);
                    hand1.Enqueue(v2);
                    if (hand2.Count == 0)
                        winner = hand1;
                }
                else
                {
                    hand2.Enqueue(v2);
                    hand2.Enqueue(v1);
                    if (hand1.Count == 0)
                        winner = hand2;
                }
            }

            public void PlayRecursiveGame()
            {
                while (InProgress)
                    PlayRecursiveRound();
            }
            
            private void PlayRecursiveRound()
            {
                if (!handsState.Add(HashCode.Combine(hand1.GetHashCode(), hand2.GetHashCode())))
                {
                    winner = hand1;
                    return;
                }

                var v1 = hand1.Dequeue();
                var v2 = hand2.Dequeue();

                bool roundWinnerIsPlayer1;
                if (hand1.Count >= v1 && hand2.Count >= v2)
                {
                    var subGame = Clone(v1, v2);
                    if (subGame.hand1.Max() > subGame.hand2.Max())
                        roundWinnerIsPlayer1 = true;
                    else
                    {
                        subGame.PlayRecursiveGame();
                        roundWinnerIsPlayer1 = ReferenceEquals(subGame.winner, subGame.hand1);
                    }
                }
                else
                    roundWinnerIsPlayer1 = v1 > v2;

                if (roundWinnerIsPlayer1)
                {
                    hand1.Enqueue(v1);
                    hand1.Enqueue(v2);
                    if (hand2.Count == 0)
                        winner = hand1;
                }
                else
                {
                    hand2.Enqueue(v2);
                    hand2.Enqueue(v1);
                    if (hand1.Count == 0)
                        winner = hand2;
                }
            }

            private bool InProgress => winner is null;

            public long GetScore()
            {
                if (InProgress)
                    throw new InvalidOperationException("The game is still in progress");
                
                var count = winner!.Count;
                var tmp = winner.ToArray();
                var result = 0;
                for (var i = count; i > 0; i--)
                    result += tmp[^i] * i;
                return result;
            }
        }
        
        [TestCase(TestInput, ExpectedResult = 306)]
        [TestCase(RealInput, ExpectedResult = 35370)]
        public static long Part1(in string input)
        {
            var game = Game.Parse(input);
            game.PlayNormalGame();
            return game.GetScore();
        }

        [TestCase(TestInput, ExpectedResult = 291)]
        [TestCase(TestInput2, ExpectedResult = 105)]
        [TestCase(RealInput, ExpectedResult = 36246)]
        public static long Part2(in string input)
        {
            var game = Game.Parse(input);
            game.PlayRecursiveGame();
            return game.GetScore();
        }

        private const string TestInput = @"
Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10
";

        private const string TestInput2 = @"
Player 1:
43
19

Player 2:
2
29
14
";

        private const string RealInput = @"
Player 1:
25
37
35
16
9
26
17
5
47
32
11
43
40
15
7
19
36
20
50
3
21
34
44
18
22

Player 2:
12
1
27
41
4
39
13
29
38
2
33
28
10
6
24
31
42
8
23
45
46
48
49
30
14
";
    }
}